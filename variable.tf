variable "bucket_name" {
  type        = string
  description = "Bucket name"
}

variable "project" {
  type = string
}

variable "location" {
  type    = string
  default = "asia-southeast1"
}

variable "uniform_level_access" {
  type = bool
  default = false
}