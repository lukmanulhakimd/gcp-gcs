resource "google_storage_bucket" "bucket" {
  name     = var.bucket_name
  project  = var.project
  location = var.location

  uniform_bucket_level_access = var.uniform_level_access
}
